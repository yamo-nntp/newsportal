<?php
/*  Newsportal NNTP<->HTTP Gateway
 *  Download: http://florian-amrhein.de/newsportal
 *
 *  Copyright (C) 2002-2004 Florian Amrhein
 *  E-Mail: florian.amrhein@gmx.de
 *  Web: http://florian-amrhein.de
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
include "config.inc.php";
@$fieldnamedecrypt=$_REQUEST['fielddecrypt'];
@$newsgroups=$_REQUEST["newsgroups"];
@$type=$_REQUEST["type"];
@$subject=$_REQUEST[md5($fieldnamedecrypt."subject")];
@$name=$_REQUEST[md5($fieldnamedecrypt."name")];
@$email=$_REQUEST[md5($fieldnamedecrypt."email")];
@$body=$_REQUEST[md5($fieldnamedecrypt."body")];
@$abspeichern=$_REQUEST["abspeichern"];
@$references=$_REQUEST["references"];
@$id=$_REQUEST["id"];
if (!isset($group)) $group=$newsgroups;

include "auth.inc";
if($post_captcha)
  include "lib/captcha/captcha.php";
$form_noemail=false;
$form_noname=false;
// Save name and email in cookies
if (($setcookies==true) && (isset($abspeichern)) && ($abspeichern=="ja")) {
  setcookie("cookie_name",stripslashes($name),time()+(3600*24*90));
  setcookie("cookie_email",$email,time()+(3600*24*90));
} 
if ((isset($post_server)) && ($post_server!=""))
  $server=$post_server;
if ((isset($post_port)) && ($post_port!=""))
  $port=$post_port;

include "head.inc";
include $file_newsportal;
@$mygroup=$_REQUEST["group"];
// check to which groups the user is allowed to post to
//$newsgroups=cancrossport($_REQUEST['group'],$group);
$newsgroups=cancrossport($_REQUEST['group'],$newsgroups);

// has the user write-rights on the newsgroups?
if (!user_can_read($newsgroups)) {
  die("access denied");
}

// Load name and email from cookies
if ($setcookies) {
  if ((isset($_COOKIE["cookie_name"])) && (!isset($name)))
    $name=$_COOKIE["cookie_name"];
  if ((isset($_COOKIE["cookie_email"])) && (!isset($email)))
    $email=$_COOKIE["cookie_email"];
}

// Load name and email from the registration system, if available
if(function_exists("npreg_get_name")) {
  $name=npreg_get_name();
}

if(function_exists("npreg_get_email")) {
  global $form_noemail;
  $email=npreg_get_email();
  $form_noemail=true;
}

if((!isset($references)) || ($references=="")) {
  $references=false;
}

if (!isset($type)) {
  $type="new";
}

if ($type=="new") {
  $subject="";
  $bodyzeile="";
  $show=1;
}



// Is there a new article to be bost to the newsserver?
if ($type=="post") {
  $show=0;
  // error handling
  if (trim($body)=="") {
    $type="retry";
    $error=$text_post["missing_message"];
  }
  if ((trim($email)=="") && (!isset($anonym_address))) {
    $type="retry";
    $error=$text_post["missing_email"];
  }
  if (($email) && (!validate_email(trim($email)))) {
    $type="retry";
    $error=$text_post["error_wrong_email"];
  }
  if (trim($name)=="") {
    $type="retry";
    $error=$text_post["missing_name"];
  }
  if (trim($subject)=="") {
    $type="retry";
    $error=$text_post["missing_subject"];
  }

  // captcha-check
  if(($post_captcha) && (captcha::check()==false)) {
    $type="retry";
    $error=$text_post["captchafail"];
  }
 $replyto="";
  if ($type=="post") {
    if (!$readonly) {
      // post article to the newsserver
      if($references)
        $references_array=explode(" ",$references);
      else
        $references_array=false;
      if(($email=="") && (isset($anonym_address))){
        $nemail=$anonym_address;
		} else {
		  $nemail=$email;
		  if ( isset($anonym_address) && ($set_replyto == true) ){
				$nemail=$anonym_address;
				$replyto=$email;
 			}
      }
      $message=message_post($subject,
                HeaderTools::qp_encode($name) ." <" . $nemail . ">",
		$replyto,$newsgroups,$references_array,addslashes($body));
      // Article sent without errors, or duplicate?
      if ((substr($message,0,3)=="240") ||
          (substr($message,0,7)=="441 435")) {
  			echo '<h1 class="np_post_headline">';
			echo  $text_post["message_posted_on"];
			echo  htmlspecialchars($newsgroups);
			echo "</h1>\n<p>";
			echo $text_post["message_posted2"];
			echo '</p><p><a href="';
			echo $file_thread.'?group=';
			echo  urlencode($mygroup) . '">';
			echo $text_post["button_back"] . '</a> ';
			echo $text_post["button_back2"];
			echo $text_post["button_back3before"];
			echo  htmlspecialchars($mygroup);
			echo $text_post["button_back3after"];
			echo "</p>\n<p>";
			echo $text_post["message_posted3"];
			echo '<a href="index.php">';
			echo $text_post["message_posted4"];
			echo "</a>\n</p>";
      } else {
        // article not accepted by the newsserver
        $type="retry";
        $error="<span class='notwanted rouge'>"
         . $text_post["error_newsserver"] 
         . '</span><br><pre class="notwanted">'
         . $message . '</pre>';
      }
    } else {
      echo '<span class="error rouge">'
       . $text_post["error_readonly"]
       . "</span>";
    }
  }
} // $type=="post"
 
// A reply of another article.
if ($type=="reply" && trim($error) == '') {

  $message=message_read($id,0,$newsgroups);
  $head=$message->header;
  $body=explode("\n" , $message->body[0]);
  nntp_close($ns);
  if ($head->name != "") {
    $bodyzeile=headerDecode2($head->name);
  } else {
    $bodyzeile=headerDecode2($head->from);
  }
  $bodyzeile=$text_post["wrote_prefix"].$bodyzeile.
             $text_post["wrote_suffix"]."\n";
  for ($i=0; $i<=count($body)-1; $i++) {
    if((isset($cutsignature)) && ($cutsignature==true) &&
       ($body[$i]=='-- '))
      break;
    if (trim($body[$i])!="") {
      if($body[$i][0]=='>')
        $bodyzeile.=">".$body[$i]."\n";
      else
        $bodyzeile.="> ".$body[$i]."\n";
    } else {
			/*
			 * if not first line or count-i line
			 * TODO write better code
			 */
			if( $i > 0 && $i < count($body)-1){
			   if ( ($body[$i-1][0]=='>' && $body[$i+1][0]=='>')
				|| ($body[$i-1][0]=='>' && $body[$i+1][1]=='>')
				|| ($body[$i-1][1]=='>' && $body[$i+1][0]=='>')
				|| ( (trim($body[$i-1]) =="") && $body[$i+1][0]=='>') )
				$bodyzeile.="> \n";
			   else
				$bodyzeile.="\n";
			} else {
		      		$bodyzeile.="\n";
			}
    }
  }
  $subject=$head->subject;
  if (isset($head->followup) && ($head->followup != "")) {
    $newsgroups=cancrossport($_REQUEST['group'],$head->followup);
  }
   
  
  
  splitSubject($subject);
  $subject="Re: ".$subject; // Do not change it!
  // Cut off old parts of a subject
  // for example: 'foo (was: bar)' becomes 'foo'.
  $subject=preg_replace('/(\(wa[sr]: .*\))$/i' , '' , $subject);
  $subject=preg_replace('/(\(Etait: .*\))$/i' , '' , $subject); //(Était: bar) doesn't work!
  //TODO add other languages.
  $show=1;
  $references=false;
  if (isset($head->references[0])) {
    for ($i=0; $i<=count($head->references)-1; $i++) {
      $references .= $head->references[$i]." ";
    }
  }
  $references .= $head->id;
}

if ($type=="retry") {
  $show=1;
  $bodyzeile=$body;
}

if ($show==1) {	
	if ($newsgroups == "") {
		  echo '<p class="error">' . $text_post["followup_not_group"];
		  echo '</p><p>'. $text_post["backto"] . '<a href="' ;
		  echo $previous . '">'.$text_post["previous"]  . '</a>.</p>';
	} else {
		  // show post form
		  $fieldencrypt=md5(rand(1,10000000));		
		  echo '<h1 class="np_post_headline">';
		  echo $text_post["group_head"].$newsgroups;
		  echo $text_post["group_tail"];
		  echo '</h1><p id="p_group_head2">';
		  echo $text_post["group_head2"] . '</p>';
		
		  if (isset($error))
			  echo "<p class='error'>$error</p>";
		  echo '<form action="';
		  echo $file_post;
		  echo '" method="post" name="postform">';
		  echo '<div class="np_post_header">';
		  echo '<table>';
		  echo '<tr><td class="droite">';
		  echo '<b>';
		  echo $text_header["subject"];
		  echo ' </b></td><td><input  placeholder="Sujet" type="text" name="';
		  echo md5($fieldencrypt."subject"). '" value="' ;
		  echo  htmlentities($subject);
		  echo '" size="40" maxlength="600" /></td></tr>' ;
		  echo '<tr><td class="droite"><b>';
		  echo $text_post["name"];
		  echo '</b></td>';
		  echo ' <td class="gauche">';
		 if($form_noname===true) {
		   echo htmlspecialchars($name);
		 } else {
		   echo '<input type="text" name="'.md5($fieldencrypt."name").'"';
		   if (isset($name))
		    echo ' value="'. htmlspecialchars(stripslashes($name)).'"';
		   echo ' size="40" maxlength="600">';
		 }
		
		 echo'</td></tr><tr><td class="droite"><b>';
		 echo $text_post["email"];
		 echo '</b></td><td class="gauche">';
		
		
		 if($form_noemail===true) {
		   echo htmlspecialchars($email);
		 } else {
		    $placeholdermail='';
		    if ( isset($anonym_address) && isset($set_replyto) && ($set_replyto == true)){
				$placeholdermail=' placeholder="' .$text_post["reply_to"]. '" ';
		    }
		   echo '<input '. $placeholdermail ;
		   echo ' type="text" name="';
		   echo md5($fieldencrypt."email");
		   echo '"';
		   if (isset($email)) 
		   	echo ' value="'.htmlspecialchars(stripslashes($email)) . '"';
		   echo ' size="40" maxlength="40" />';
		 }
		 echo " </td></tr>\n</table>\n</div>\n";
		 echo '<div class="np_post_body">';
		 echo "<table>\n<tr><td><b>";
		 echo $text_post["message"];
		 echo '</b><br><textarea id="postbody" name="';
		 echo md5($fieldencrypt."body"); 
		 echo '" rows="20" cols="79" wrap="virtual">';
		 
		if ((isset($bodyzeile)) && ($post_autoquote))
		  echo htmlspecialchars($bodyzeile);
		if(is_string($body) &&  trim($error) == '')
		  echo htmlspecialchars($body);
		 echo '</textarea></td></tr>';
		 echo '<tr><td>';		
		if(!$post_autoquote) {
				echo ' <input type="hidden" id="hidebody" value="';
				if (isset($bodyzeile)) echo htmlspecialchars(stripslashes($bodyzeile)) ;
				echo '"><script language="JavaScript">';
				echo "<!--\nfunction quoten() {\n";
				echo 'document.getElementById("postbody").value=document.getElementById("hidebody").value;';
				echo "\n";
				echo 'document.getElementById("hidebody").value="";';
				echo "\n}\n//-->\n</script>";
				echo '<input tabindex="100" type="Button" name="quote" value="';
				echo $text_post["quote"];
				echo 'onclick="quoten()">';
		 } 
		 echo ' <input type="submit"  value="';
		 echo $text_post["button_post"];
		 echo '"> ';
		 if ($setcookies==true) { 
		 		echo '<span class="ckie"> <input type="checkbox" name="abspeichern" value="ja">';
		 		echo  $text_post["remember"] . '</span>';
		 } 
		 $previous = $file_thread.'?group='. urlencode($mygroup);
		 echo '<span class="spacer"></span><a class="button" href="';
		 echo $previous . '">' . $text_article["button_back"]  . '</a>';
		 echo '</td>';
		 echo '</tr>';
		 if($post_captcha) {
			  echo '<tr><td>';
			  echo captcha::form( $text_post["captchainfo1"], $text_post["captchainfo2"] );
			  echo '</td></tr>';
		} 
		 echo '</table>';
		 echo '</div> <input type="hidden" name="type" value="post">';
		 echo ' <input type="hidden" name="newsgroups" value="';
		 echo htmlspecialchars($newsgroups);
		 echo '">';
		 echo ' <input type="hidden" name="references" value="';
		 echo htmlentities($references);
		 echo '">';
		 echo ' <input type="hidden" name="group" value="';
		 echo htmlspecialchars($newsgroups);
		 echo '">';
		 echo ' <input type="hidden" name="fielddecrypt" value="';
		 echo htmlspecialchars($fieldencrypt);
		 echo '">';
		 echo '</form>';
	  } 
} 
include "tail-post.inc";
?>
