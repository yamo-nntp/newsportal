# modification of /etc/news/filter/filter_nnrpd.pl
use Digest::SHA qw( sha256_base64 hmac_sha256_base64 );
$CANCEL_LOCK = "chaine688secrete15aleatoire"; #to be updated!

#local stuff :
my %config = (checkincludedtext => 1,
              includedcutoff => 60,
              includedratio => 0.6,
              quotere => '^[>:]',
              antiquotere => '^[<]',  # so as not to reject dict(1) output
             );


sub filter_post {
    my $rval = "" ;             # assume we'll accept.
    ############################
    # dans tous les cas : modify_headers (cancel-lock & cancel-key)
        $modify_headers = 1;
    #############################
    if ($config{checkincludedtext}) {
        my ($lines, $quoted, $antiquoted) = analyze($body);
        if ($lines > $config{includedcutoff}
                && $quoted - $antiquoted > $lines * $config{includedratio}) {
#            $rval = "Article contains too much quoted text";
            $rval = "Il y a trop de citations cf : http://www.usenet-fr.net/fur/usenet/repondre-sur-usenet.html ";
        }
    }
#
# newsportal
#
#myheader_secret: secret
# warning : You have to edit : myheader_secret and secret (exactly as in config.inc.php)
my  $your_fqdn = "fqdn.localhost.local"; # to be updated!
my  $your_abuse_fqdn = 'abuse@fqdn.localhost.local'; # to be updated!
 if ($hdr{'myheader_secret'} =~ /secret/i) {
        my $http_host = $hdr{'Http-Posting-Host'};
        $user = $hdr{'Newsportal-User'};
        $hdr{'Injection-Info'} = $your_fqdn . "; posting-account="
                . '"' . $user . '"; ' . "\n " . 'posting-host="' . $http_host
                . '" logging-data="http"; ' . "\n " . 'mail-complaints-to="' .$your_abuse_fqdn .'"';
        $hdr{'myheader_secret'}      = undef; # to be updated!
        $hdr{'Http-Posting-Host'}  = undef;
        $hdr{'Newsportal-User'}    = undef;
         #pour test à commenter en production :
        $hdr{'X-User'}     = $user;
  }
  # Cancel-Lock / Cancel-Key
  # https://home.gegeweb.org/rfc8315.html https://www.bortzmeyer.org/8315.html
   add_cancel_lock(\%hdr, $user);

   if (exists( $hdr{"Control"} ) && $hdr{"Control"} =~ m/^cancel\s+(<[^>]+>)/i) {
      my $key = calc_cancel_key($user, $1);
      add_cancel_item(\%hdr, 'Cancel-Key', $key);
   }
   elsif (exists( $hdr{"Supersedes"} )) {
      my $key = calc_cancel_key($user, $hdr{"Supersedes"});
      add_cancel_item(\%hdr, 'Cancel-Key', $key);
   }


    return $rval;
} #end of filter_post
    

#
# quotage
#
sub analyze {
    my ($lines, $quoted, $antiquoted) = (0, 0, 0);
    local $_ = shift;

    do {
        if ( /\G$config{quotere}/mgc ) {
            $quoted++;
        } elsif ( /\G$config{antiquotere}/mgc ) {
            $antiquoted++;
        }
    } while ( /\G(.*)\n/gc && ++$lines );

    return ($lines, $quoted, $antiquoted);
}

# https://home.gegeweb.org/rfc8315.html
# <slrnsgkh1f.10v9.gerald.niel+spam@home.niel.me>
#
# Cancel-Lock / Cancel-Key
#

sub add_cancel_item($$$) {
   my ( $r_hdr, $name, $value ) = @_;
   my $prefix = $r_hdr->{$name};
#   $prefix = defined($prefix) ? $prefix . ' sha1:' : 'sha1:';
   $prefix = defined($prefix) ? $prefix . ' sha256:' : 'sha256:';
   $r_hdr->{$name} = $prefix . $value;
}

#
# Cancel-Lock / Cancel-Key
#
sub calc_cancel_key($$) {
   my ( $user, $message_id ) = @_;
   return pad_b64digest(hmac_sha256_base64($message_id, $user . $CANCEL_LOCK));
#   return MIME::Base64::encode(Digest::HMAC_SHA1::hmac_sha1($message_id, $user . $CANCEL_LOCK), '');
}

#
# Cancel-Lock / Cancel-Key
#
sub add_cancel_lock($$) {
   my ( $r_hdr, $user ) = @_;
   my $key = calc_cancel_key($user, $r_hdr->{'Message-ID'});
   my $lock = pad_b64digest(sha256_base64($key));
   #my $lock = MIME::Base64::encode(Digest::SHA::sha1($key), '');
   add_cancel_item($r_hdr, 'Cancel-Lock', $lock);
}

#
# Cancel-Lock / Cancel-Key
#
sub pad_b64digest($) {
    my ($b64_digest) = @_;
    while (length($b64_digest) % 4) {
        $b64_digest .= '=';
    }
    return $b64_digest;
}


sub filter_end {
# Do whatever you want to clean up things when Perl filtering is disabled.
}





