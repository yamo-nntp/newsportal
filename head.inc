<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $myHTML_lang; ?>">
<head>
<title><?php echo htmlspecialchars($title); ?></title>
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=<?php echo $www_charset; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<?php
if (isset ($description) && $description != ""){
	echo '<meta name="description" content="';
	echo $description; 
	echo '"/>';
}else{
	echo '<meta name="description" content="';
	echo $www_description; 
	echo '"/>';
}
?>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<a id="top"></a>
