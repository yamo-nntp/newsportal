<?php

/*
 *  Copyright (C) 2020 Olivier Miakinen
 *  E-Mail: om+newsportal@miakinen.net
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/****************************************************************************
*****************************************************************************

This file defines three classes.

The class 'HeaderTools' contains static functions only (all of them
are public) which help to encode and decode header fields in accordance
with RFC 2047.

The purpose of the class 'MimeEncodeHeader' is to encode a complete
unstructured header such as Subject, Comments, Summary or Organization.
The distinction between structured and unstructured headers is to be
found in RFC 5536 or 5322.

The purpose of the class 'MimeDecodeHeader' is to decode a complete
header which may (or may not) contain MIME encoded-words.

Notes:

  Although the class MimeEncodeHeader shall not be used for structured
  headers such as From or Reply-To because the address specification
  (in angle brackets) must not be encoded, the display name part may
  be encoded by using encoding functions from the HeaderTools class.
  This also applies to any parenthesized comment.

  Conversely, the class MimeDecodeHeader should normally be safe with
  any header because it would be extremely unlikely that a token in a
  structured header would contain what looks like a MIME encoded-word.

*****************************************************************************

Class HeaderTools

  function sanitize_no_control($str)
    This function purges the string of all control characters (\x00..\x1f
    or \x7f), including header folding CR+LFs. Additionally, any sequence
    of multiple white characters becomes a single space.

  function sanitize_utf8($str)
    This function ensures that the string is in valid UTF-8. If necessary,
    it converts it from CP1252 (Windows superset of ISO-8859-1) to UTF-8.

  function qp_encoded_len($str)
    This function tells which length would be the result of the
    encoding of $str using quoted-printable and UTF-8.
    It is guaranteed that HeaderTools::qp_encoded_len($str) returns
    the same result as strlen(HeaderTools::qp_encode($str))

  function b64_encoded_len($str)
    This function tells which length would be the result of the
    encoding of $str using base64 and UTF-8.
    It is guaranteed that HeaderTools::b64_encoded_len($str) returns
    the same result as strlen(HeaderTools::b64_encode($str))

  function can_encode($str, $space, &$with_b64_only)
    This function returns TRUE if the string $str can be encoded,
    using either quoted-printable or base64, in no more than $space
    octets.
    In case of success, the parameter $with_b64_only will also be set:
    - to FALSE if the encoding is possible in quoted-printable;
    - to TRUE if the encoding is only possible in base64.

  function qp_encode($str)
    This function returns the result of the encoding of $str in
    quoted-printable and UTF-8, in the form "=?UTF-8?Q?...?="

  function b64_encode($str)
    This function returns the result of the encoding of $str in
    base64 and UTF-8, in the form "=?UTF-8?B?...?="

  function mime_encode($str, $with_b64)
    This is just a handy function which calls qp_encode or b64_encode
    according to the parameter $with_b64.

  function qp_decode($qp_string)
    This function tries to decode the encoded-text portion of a MIME
    encoded-word with encoding="Q". Return the result of the decoding
    in case of success, or the boolean value FALSE in case of error.
    Note that no charset interpretation is made at this point.

  function b64_decode($b64_string)
    This function tries to decode the encoded-text portion of a MIME
    encoded-word with encoding="B". Return the result of the decoding
    in case of success, or the boolean value FALSE in case of error.
    Note that no charset interpretation is made at this point.

  function mime_decode($str, &$charset)
    This function first tries to recognize the pattern of a MIME encoded-word:
      encoded-word = "=?" charset "?" encoding "?" encoded-text "?="
    In case of success, this function updates the parameter $charset, then
    it returns the result of qp_decode or b64_decode on the encoded-text,
    depending on whether the encoding is "Q" or "B"; otherwise, the function
    returns FALSE.
    Note that no charset interpretation is made at this point.

  function mime_decode_and_iconv($str)
    This is just a handy function which calls iconv() after mime_decode().
    Note that the charset interpretation IS made at this point.

*****************************************************************************

Class MimeEncodeHeader

Only two functions within this class have the visibility 'public':
the constructor __construct() and the magic function __toString() for
returning the result of the encoding. All other functions have the
visibility 'private' and can't be used from outside the class.

  function __construct($header_name, $header_body [, $crlf="\r\n"])
    $header_name:
      The name of the header field, e.g. "Subject" or "Organization".
      It is important that the function knows the name of the header
      field, because it impacts the length of the first line.
      Note that this $header_name MUST be a valid header field name,
      necessarily in US-ASCII, and it will never be encoded.
    $header_body:
      The body of the header field, which can possibly be encoded.
    $crlf:
      Used when the header field body is too long and has to be folded,
      the parameter $crlf specifies which line termination to use.
      This parameter is optional, and its default value is "\r\n"
      (i.e. CR + LF). But of course, if you adapt this code for your
      own use you may change the default value as needed.

  function __toString()
    This function with no parameter returns the result of the encoding.
    You may of course call it by its name, but this is a 'magical'
    function which is called whenever you use an object as if it was
    a string.

*****************************************************************************

MimeEncodeHeader general working principle

On each line of the header field, we put things in ASCII for as long as
we can. Then, when we have to switch to MIME encoding, we continue to
encode until the end of the line.

*****************************************************************************

MimeEncodeHeader detailed working principle

The first step consists to sanitize the field body so that it contains
valid UTF-8 only with no control characters, then to split this field
body into words. A word is any non-empty sequence of octets with
hexadecimal value strictly greater than \x20.

Then, the following algorithm is used.

At each line of the (possibly folded) header field, we first try to put
as many ASCII-only words as possible, with the limit of 76 characters
per line.

When a non-ASCII word is found and there is still some place left for it,
we try to encode as many other complete words as possible in the same
MIME encoded-word, with the same limit of 76 characters per line. It may
happen that ASCII-only words be encoded after non-ASCII words in the same
line.

Is there is no space left for other words (either encoded or not) and if
it is allowed to wrap at that location, a line termination is inserted
which will be followed by a space and the rest of the encoding. The only
places where it is forbidden to wrap are at the beginning of the header
field body (just after the header field name and the colon) and just
after a previous wrap.

If the following word is too long and it is forbidden to wrap, the
behaviour depends on the kind of word: if it is an ASCII-only word, never
mind, we put it anyway; but if the word is non-ASCII we first try to
encode only part of the word. If we really can't encode even one character
of the word, it's really bad luck (it should never occur anyway), but too
bad, we will put the minimum possible even if it exceeds the limit.

Note that it may happen that two MIME encoded-words appear in succession,
only separated by a folding white space (crlf + space). In that case,
if the new encoded-word corresponds to a new source word, then a space
must be encoded at its beginning. Of course, no space will be encoded
if a single source word was split into more than one encoded-words.
Also, no space will be encoded after or before a non-encoded ASCII word.

*****************************************************************************

Class MimeDecodeHeader

Only two functions within this class have the visibility 'public':
the constructor __construct() and the magic function __toString() for
returning the result of the encoding. All other functions have the
visibility 'private' and can't be used from outside the class.

  function __construct($header_field)
    The $header_field parameter could be any string containing zero or more
    encoded-words to be decoded, be it a full header or just the body of
    the header, or even just the content of a comment in parentheses.

  function __toString()
    This function with no parameter returns the result of the decoding.
    You may of course call it by its name, but this is a 'magical'
    function which is called whenever you use an object as if it was
    a string.

*****************************************************************************

MimeDecodeHeader general working principle

Decoding is much easier than encoding because there is no need to worry
about the length of the lines: just decode word after word.
The only difficulty is to get around most of the bugs that can be
encountered in existing news articles.

*****************************************************************************

MimeDecodeHeader detailed working principle

The first step consists to sanitize the input so that it contains valid
UTF-8 only with no control characters, then to split this input into words.
A word is any non-empty sequence of octets with hexadecimal value strictly
greater than \x20.

Then, the following algorithm is used for each word.

In case of a MIME encoded-word, it is first decoded and converted to UTF-8,
otherwise it is written as is.

Between two words, a space character is inserted when one or both is an
unencoded word. No space character is inserted when both were MIME
encoded-words.

Special case: when the conversion of a MIME encoded-word to UTF-8 fails,
this can be the result of a violation of this rule of RFC 2047:
   Each 'encoded-word' MUST represent an integral number of characters.
   A multi-octet character may not be split across adjacent 'encoded-
   word's.
In that case, we retry after concatenating adjacent encoded-words with
same charset and convert the result at once. See below the list of
possible bugs that we do (or don't) work around.

*****************************************************************************

Bugs that were already found in news articles and that we work around

  Bug #1 (e.g. MicroPlanet-Gravity/2.9.15)
    Some old or misconfigured newsreaders post articles with 8-bit characters
    in the headers when there should be only ASCII. This is a problem because
    there is no foolproof way to guess which charset was used.
  Workaround #1
    The most commonly encountered character set for this bug, at least on
    usenet-fr, is CP1252 (Windows Latin1) which is a superset of ISO-8859-1
    (ISO Latin1). However, UTF-8 is usually so easy to recognize that we
    first assume the text is in UTF-8. If this assumption fails, we then
    convert from CP1252. This probably covers over 99% of cases. Anyway, the
    main thing is to have a unique character set in the end, which is UTF-8.

  Bug #2 (e.g. SeaMonkey/2.49.4 or Thunderbird/68.12.1)
    In quoted-printable encoding, use lower case hexadecimal digits 'a'
    through 'f' instead of upper case 'A' through 'F'.
  Workaround #2
    Ignore the case of hexadecimal digits when decoding quoted-printable.

  Bug #3 (e.g. MicroPlanet-Gravity/3.0.4)
    Have an unencoded '?' within a quoted-printable encoded-word. It should
    normally be encoded as '=3F'.
  Workaround #3
    Accept to find an unencoded '?' in the 'encoded-text' part.

  Bug #4 (e.g. NewsPortal/0.50.1)
    Have header lines with at least one MIME encoded-word where the length
    of the line is greater than 76. This was sometimes the case for the
    first line of a multiline header.
  Workaround #4
    Don't care about the length of header lines.

  Bug #5 (e.g. MesNews/1.08.06.00)
    Violation of the rule that each encoded-word must represent an integral
    number of characters.
  Workaround #5
    Concatenate the encoded-text parts of multiple adjacent encoded-words
    with the same charset, before interpreting the result according to
    this charset.

*****************************************************************************

Bugs that were not found in news articles and that we don't work around

  Unhandled bug #1
    Have 8-bit characters in an encoded-word.

  Unhandled bug #2
    Have prohibited characters (except the '?') in a quoted-printable
    encoded-word.

  Unhandled bug #3
    Have a base64 encoding whose length is not a multiple of 4.

  Unhandled bug #4
    Have prohibited characters in a base64 encoded-word.

Should any of these occur, the corresponding word would be considered
an unencoded word and treated as such.

*****************************************************************************

Usage examples for encoding

  Example #1:
    $encoded_subject = new MimeEncodeHeader("Subject", $subject, "\n");
    $headers .= $encoded_subject->__toString() . "\n";

  Example #2:
    $encoded_subject = new MimeEncodeHeader("Subject", $subject, "\n");
    $headers .= "${encoded_subject}\n";

  Example #3:
    fwrite($out, new MimeEncodeHeader("Subject", $subject) . "\r\n");

  Example #4:
    $headers .= "From: " . HeaderTools::qp_encode($from_name)
                         . " <" . $from_addr . ">\r\n";
  Example #5:
    if (preg_match("/^[A-Za-z0-9 ]+$/", $from_name)) {
      $from_display = $from_name;   // only alphanumeric or space : ok
    } else {
      $from_display = HeaderTools::qp_encode($from_name);   // better encode
    }
    fwrite($out, "From: {$from_display} <{$from_addr}>\r\n");

Usage examples for decoding

  Example #1:
    $subject = new MimeDecodeHeader($encoded_subject);
    $subject_str = $encoded_subject->__toString();

  Example #2:
    fwrite($out, new MimeDecodeHeader($encoded_subject));

  Example #3:
    $from_display_str = "" . new MimeDecodeHeader($encoded_from_display);

*****************************************************************************
****************************************************************************/

class HeaderTools
{
  const B64_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" .
                    "abcdefghijklmnopqrstuvwxyz" .
                    "0123456789+/";
  const BEGIN_QP = "=?UTF-8?Q?";
  const BEGIN_B64 = "=?UTF-8?B?";
  const END_ANY = "?=";
  const LEN_ANY = 12; // strlen(BEGIN_xx) + strlen(END_ANY)

  public static function sanitize_no_control($str) {
    // Replace any sequence of one or more space (\x20) or control
    // character (\x00..\x1f or \x7f) by a single space.
    return preg_replace("/[\\x00-\\x20\\x7F]+/", " ", $str);
  }

  public static function sanitize_utf8($str) {
    // Check that the string is valid UTF-8
    if (preg_match('//u', $str)) return $str;
    // If it's not, do our best to convert it to UTF-8
    return iconv("CP1252", "UTF-8//IGNORE", $str);
  }

  public static function qp_encoded_len($str) {
    $len = strlen($str);
    $qp_len = self::LEN_ANY;
    for ($idx = 0; $idx < $len; $idx++) {
      $code = ord($str[$idx]);
      if ($code == 63 || $code == 95) {
        // '?' and '_' are ASCII but must be encoded in QP
        $qp_len += 3;
      } else if ($code >= 32 && $code <= 126) {
        // Other ASCII characters (including ' ' encoded as '_')
        $qp_len += 1;
      } else {
        // Octets outside range of printable ASCII
        $qp_len += 3;
      }
    }
    return $qp_len;
  }

  public static function b64_encoded_len($str) {
    $len = strlen($str);
    return self::LEN_ANY + ((int) ceil((float) $len / 3.0) * 4);
  }

  public static function can_encode($str, $space, &$with_b64_only) {
    if (self::qp_encoded_len($str) <= $space) {
      $with_b64_only = FALSE;
      return TRUE;
    }
    if (self::b64_encoded_len($str) <= $space) {
      $with_b64_only = TRUE;
      return TRUE;
    }
    return FALSE;
  }

  public static function qp_encode($str) {
    $len = strlen($str);
    $qp_string = self::BEGIN_QP;

    for ($idx = 0; $idx < $len; $idx++) {
      $char = $str[$idx];
      $code = ord($char);
      if ($code == 63 || $code == 95) {
        // '?' and '_' are ASCII but must be encoded in QP
        $qp_string .= sprintf("=%02X", $code);
      } else if ($code == 32) {
        // The space ' ' is encoded as '_'
        $qp_string .= "_";
      } else if ($code >= 32 && $code <= 126) {
        // Other ASCII characters
        $qp_string .= $char;
      } else {
        // Octets outside range of printable ASCII
        $qp_string .= sprintf("=%02X", $code);
      }
    }
    return $qp_string . self::END_ANY;
  }

  public static function b64_encode($str) {
    $len = strlen($str);
    $b64_string = self::BEGIN_B64;

    while ($len >= 3) {
      $code = (ord($str[0]) << 16) + (ord($str[1]) << 8) + ord($str[2]);
      $b64_string .= self::B64_TABLE[($code >> 18) & 0x3F];
      $b64_string .= self::B64_TABLE[($code >> 12) & 0x3F];
      $b64_string .= self::B64_TABLE[($code >> 6) & 0x3F];
      $b64_string .= self::B64_TABLE[$code & 0x3F];
      $str = substr($str, 3);
      $len -= 3;
    }
    switch ($len) {
    case 2:
      $code = (ord($str[0]) << 16) + (ord($str[1]) << 8);
      $b64_string .= self::B64_TABLE[($code >> 18) & 0x3F];
      $b64_string .= self::B64_TABLE[($code >> 12) & 0x3F];
      $b64_string .= self::B64_TABLE[($code >> 6) & 0x3F];
      $b64_string .= "=";
      break;
    case 1:
      $code = (ord($str[0]) << 16);
      $b64_string .= self::B64_TABLE[($code >> 18) & 0x3F];
      $b64_string .= self::B64_TABLE[($code >> 12) & 0x3F];
      $b64_string .= "==";
      break;
    }

    return $b64_string . self::END_ANY;
  }

  public static function mime_encode($str, $with_b64) {
    return ($with_b64) ? self::b64_encode($str) : self::qp_encode($str);
  }

  public static function qp_decode($qp_string) {  	
    if(is_null($qp_string) || empty($qp_string)) {
      return FALSE;
    }
    if (preg_match("/[^\\x21-\\x7e]/", $qp_string)) {
      // The string contains a character outside of printable ASCII
      // characters or a space.
      return FALSE;
    }

    $str = "";

    while (($qp_string !== "") && ($qp_string !== FALSE)) {
      $char = $qp_string[0];

      if ($char === "=") {
        // Should be an octet encoded in hexadecimal =XX
        if (strlen($qp_string) < 3) return FALSE;
        $char = hex2bin(substr($qp_string, 1, 2));
        if ($char === FALSE) return FALSE;
        $str .= $char;
        $qp_string = substr($qp_string, 3);
        continue;
      }

      if ($char === "_") {
        // Special case for space encoded as underscore
        $char = " ";
      }

      // Note that even the '?' is accepted to take into account
      // the bug of MicroPlanet Gravity.
      $str .= $char;
      $qp_string = substr($qp_string, 1);
    }
    return $str;
  }

  public static function b64_decode($b64_string) {  	
    if(is_null($b64_string) || empty($b64_string)) {
      return FALSE;
    }
    $b64_len = strlen($b64_string);
    if (($b64_len % 4) != 0) {
      return FALSE;
    }
    if (!preg_match("#^[A-Za-z0-9+/]*={0,2}$#", $b64_string)) {
      return FALSE;
    }

    $str = "";
    while ($b64_string !== "") {
      $char0 = $b64_string[0];
      $pos0 = strpos(self::B64_TABLE, $char0);
      if ($pos0 === FALSE) return FALSE;
      $code = $pos0 << 18;

      $char1 = $b64_string[1];
      $pos1 = strpos(self::B64_TABLE, $char1);
      if ($pos1 === FALSE) return FALSE;
      $code += $pos1 << 12;
      $str .= chr($code >> 16);

      $char2 = $b64_string[2];
      if ($char2 === "=") break;
      $pos2 = strpos(self::B64_TABLE, $char2);
      if ($pos2 === FALSE) return FALSE;
      $code += $pos2 << 6;
      $str .= chr($code >> 8);

      $char3 = $b64_string[3];
      if ($char3 === "=") break;
      $pos3 = strpos(self::B64_TABLE, $char3);
      if ($pos3 === FALSE) return FALSE;
      $code += $pos3;
      $str .= chr($code);

      $b64_string = substr($b64_string, 4);
    }
    return $str;
  }

  public static function mime_decode($str, &$charset) {
    if (! preg_match("/^=\?(.*)\?([QqBb])\?(.*)\?=$/", $str, $matches)) {
      // This is definitely not an encoded-word
      return FALSE;
    }
    $charset = $matches[1]; // returned to caller
    $encoding = $matches[2];
    $encoded_text = $matches[3];
    switch ($encoding) {
    case 'Q': case 'q':
      return HeaderTools::qp_decode($encoded_text);
    case 'B': case 'b':
      return HeaderTools::b64_decode($encoded_text);
    }
    // Encoding is neither 'Q' nor 'B'
    return FALSE;
  }

  public static function mime_decode_and_iconv($str) {
    $decoded_text = self::mime_decode($str, $charset);
    if ($decoded_text === FALSE) {
      // This was not a valid encoded-word
      return FALSE;
    }
    return @iconv($charset, "UTF-8", $decoded_text);
  }
}

/***************************************************************************/

class MimeEncodeHeader
{
  const MAX_LEN = 76;   // According to RFC 2047
  private $crlf;
  private $words;
  private $word_count;
  private $word_idx = 0;
  private $char_idx = 0;
  private $left_in_line;
  private $may_wrap = FALSE;
  private $prev_encoded = FALSE;
  private $result = "";

  public function __construct($header_name, $header_body, $crlf="\r\n") {
    $this->result = "{$header_name}:";
    $this->left_in_line = self::MAX_LEN - strlen($this->result);

    /*
     * First of all, check that the header body is a valid UTF-8 string
     * without any control character, and split this body into a list
     * of words.
     */
    $header_body = HeaderTools::sanitize_no_control($header_body);
    $header_body = HeaderTools::sanitize_utf8($header_body);
    $this->words = preg_split("/ /", $header_body, -1, PREG_SPLIT_NO_EMPTY);
    $this->word_count = count($this->words);

    $this->crlf = $crlf;

    while ($this->word_idx < $this->word_count) {
      /*
       * First we check if the next word is ASCII and we handle this case.
       * Note that it is only possible if there is no pending partial
       * encoding (i.e. if $this->char_idx is 0)
       */
      if ($this->char_idx == 0 && $this->next_is_ascii()) {
        if ($this->ok_ascii()) {
          // There is enough room for the next ASCII word: write it.
          $this->write_ascii();
          continue;
        }
        if ($this->may_wrap) {
          // There isn't enough room for the next ASCII word, but it is
          // authorized to wrap: do it, it can only improve things.
          $this->wrap();
        }
        // Whether or not we wrapped, never mind if the word is too long,
        // now write it anyway.
        $this->write_ascii();
        continue;
      }

      /*
       * The next word is not ASCII, or there is a pending encoding,
       * which means that we have to continue in encoded form.
       * First try to encode complete words (possibly after the end
       * of a previously partial word encoding).
       */
      $w_count = $this->how_many_encodable_words($with_b64);
      if ($w_count > 0) {
        // Ok, encode this word or these words
        $this->write_encoded($w_count, $with_b64);
        continue;
      }
      if ($this->may_wrap) {
        // There isn't enough room for the next encoded word, but it is
        // authorized to wrap: do it, then restart the while loop with
        // the new value of $this->left_in_line.
        $this->wrap();
        continue;
      }

      /*
       * There is still not enough room for a single word.
       * Try with a partial encoding of the next word only.
       */
      $c_count = $this->how_many_encodable_chars($with_b64);
      if ($c_count > 0) {
        // Ok, encode these few octets of this one word.
        $this->write_partial_encoded($c_count, $with_b64);
        continue;
      }

      /*
       * Now we are really unlucky. How can it be that at the same time we
       * are not allowed to wrap and we cannot encode a single character?
       * Do it anyway: encode the smallest possible number of octets.
       */
      $this->write_minimum_encoded();
    }
  }

  public function __toString() {
    return $this->result;
  }

  private function next_word($idx = -1) {
    // if $idx is not set, we resume where we stopped, i.e. the
    // current $word_idx and the current $char_idx
    if ($idx == -1) {
      return substr($this->words[$this->word_idx], $this->char_idx);
    }
    // else it is necessarily a complete word
    return $this->words[$idx];
  }

  private function next_is_ascii() {
    // A string is ASCII if it does not contain any characters
    // outside the range 32 (space) to 126 (~).
    return (!preg_match("/[^\\x20-\\x7e]/", $this->next_word()));
  }

  private function ok_ascii() {
    return (1 + strlen($this->next_word()) <= $this->left_in_line);
  }

  private function write_ascii() {
    $add = " " . $this->next_word();
    $this->result .= $add;
    $this->left_in_line -= strlen($add);
    $this->may_wrap = TRUE;
    $this->prev_encoded = FALSE;
    $this->word_idx ++;
  }

  private function how_many_encodable_words(&$with_b64_only) {
    // How many complete words would it be possible to encode?

    $buffer = $this->next_word();   // possibly the end of a word
    if ($this->prev_encoded) {
      // If there was a completely encoded word before, we shall encode
      // the space between the previous word and the new one.
      $buffer = " " . $buffer;
    }

    $space_left = $this->left_in_line - 1;
    if (! HeaderTools::can_encode($buffer, $space_left, $with_b64_only)) {
        // There isn't even room to encode a single word
        return 0;
    }

    // Ok, we can encode a word. Can we encode more than one?
    for ($idx = $this->word_idx + 1; $idx < $this->word_count; $idx++) {
      $buffer .= " " . $this->next_word($idx);  // only a complete word
      if (! HeaderTools::can_encode($buffer, $space_left, $with_b64_only)) {
        break;  // We can no more, stop here
      }
    }
    return $idx - $this->word_idx;  // how many words can be encoded
  }

  private function write_encoded($w_count, $with_b64) {
    $buffer = $this->next_word();   // possibly the end of a word
    if ($this->prev_encoded) {
      // If there was a completely encoded word before, we shall encode
      // the space between the previous word and the new one.
      $buffer = " " . $buffer;
    }
    $idx = $this->word_idx + 1;
    while ($idx < $this->word_idx + $w_count) {
      $buffer .= " " . $this->next_word($idx);
      $idx++;
    }

    $this->result .= " " . HeaderTools::mime_encode($buffer, $with_b64);
    $this->word_idx = $idx;
    $this->char_idx = 0;
    $this->prev_encoded = TRUE;
    if ($this->word_idx < $this->word_count) {
      $this->wrap();
    }
    // There is no need to update $this->may_wrap or $this->left_in_line
  }

  private function how_many_encodable_chars(&$with_b64_only) {
    // Even the first available word is too long to be completely encoded.
    // How many chars of this first word would it be possible to encode?

    $buffer = $this->next_word();   // possibly the end of a word
    $c_count = strlen($buffer);
    if ($this->prev_encoded) {
      // If there was a completely encoded word before, we shall encode
      // the space between the previous word and the new one.
      $buffer = " " . $buffer;
    }

    $space_left = $this->left_in_line - 1;
    do {
      // While we can't encode this number of characters, we continue
      // suppressing octets.
      do {
        // While the last suppressed octet is a UTF-8 continuation octet
        // (binary 10xxxxxx) we must continue suppressing octets.
        $last_char = substr($buffer, -1);
        $buffer = substr($buffer, 0, -1);
        $c_count--;
        if ($c_count == 0) {
          // We have suppressed *all* the word! Nothing is encodable here.
          return 0;
        }
      } while ((ord($last_char) & 0xC0) == 0x80);
    } while (! HeaderTools::can_encode($buffer, $space_left, $with_b64_only));

    // Ok, now we can encode this number of characters.
    return $c_count;
  }

  private function write_partial_encoded($c_count, $with_b64) {
    $buffer = $this->next_word();           // possibly the end of a word
    $buffer = substr($buffer, 0, $c_count); // only the first $c_count octets
    if ($this->prev_encoded) {
      // If there was a completely encoded word before, we shall encode
      // the space between the previous word and the new one.
      $buffer = " " . $buffer;
    }

    $this->result .= " " . HeaderTools::mime_encode($buffer, $with_b64);
    $this->char_idx += $c_count;
    $this->prev_encoded = FALSE;  // Don't add a space in the middle of a word
    $this->wrap();  // The rest of the word will be encoded on next line
    // There is no need to update $this->may_wrap or $this->left_in_line
  }

  private function write_minimum_encoded() {
    // This last chance function should really NEVER been called!
    if ($this->prev_encoded) {
      // REALLY? We are at the beginning of a line and have no space left?
      // Ok. The minimum to encode is the space between words.
      $buffer = " ";
    } else {
      $buffer = $this->next_word();     // possibly the end of a word
      $len = strlen($buffer);
      for ($c_count = 1; $c_count < $len; $c_count++) {
        // We stop at first octet which is not a UTF-8 continuation octet
        // (binary 10xxxxxx).
        $this_char = substr($buffer, $c_count, 1);
        if ((ord($this_char) & 0xC0) != 0x80) break;
      }
      $buffer = substr($buffer, 0, $c_count); // only the first $c_count octets
      $this->char_idx += $c_count;
    }

    $this->result .= " " . HeaderTools::qp_encode($buffer);   // why not qp?
    $this->prev_encoded = FALSE;  // Don't add a space in the middle of a word
    $this->wrap();  // The rest of the word will be encoded on next line
    // There is no need to update $this->may_wrap or $this->left_in_line
  }

  private function wrap() {
    $this->result .= $this->crlf;
    $this->may_wrap = FALSE;
    $this->left_in_line = self::MAX_LEN;
  }
}

/***************************************************************************/

class MimeDecodeHeader
{
  private $analyzed_words = array();
  private $analyzed_words_2 = array();
  private $result = "";

  public function __construct($header_field) {
    /*
     * First of all, check that the input string is a valid UTF-8 string
     * without any control character, and split this string into a list
     * of words.
     */
    $header_field = HeaderTools::sanitize_no_control($header_field);
    $header_field = HeaderTools::sanitize_utf8($header_field);
    $words = preg_split("/ /", $header_field, -1, PREG_SPLIT_NO_EMPTY);

    if ($this->build_analyzed_words($words)) {
      // All is OK. Simply finalize the resulting string.
      $this->result = $this->finalize($this->analyzed_words);
    } else if ($this->build_analyzed_words_2()) {
      // Bug #5 occurred, and was worked around.
      $this->result = $this->finalize($this->analyzed_words_2);
    } else {
      // Couldn't work around the bug. Do our best with the unedited text.
      $this->result = $this->finalize($this->analyzed_words);
    }
  }

  public function __toString() {
    return $this->result;
  }

  private function build_analyzed_words($words) {
    /*
     * For each (blank-separated) word, analyze what kind it is.
     *  - It may be an unencoded word or a MIME encoded-word.
     *  - In case of an encoded-word, we get a charset, and a choice
     *    between quoted-printable and base64.
     *  - Finally, the encoded-word has to be converted to UTF-8, or
     *    we need to know if it didn't work.
     */
    $all_iconv_ok = TRUE;
    $this->analyzed_words = array();

    foreach ($words as $word) {
      unset($analyzed_word);  // else it is reused at each pass... PHP bug?
      $analyzed_word =  [
                          "word" => $word,
                          "was_encoded" => FALSE,
                          "decoded_word" => "",
                          "charset" => "",
                          "encoded_utf8_ok" => FALSE,
                          "utf8_word" => "",
                        ];
      $this->analyzed_words[] = &$analyzed_word;

      $decoded_word = HeaderTools::mime_decode($word, $charset);
      if ($decoded_word === FALSE) {
        // This is not an encoded-word, either because it was not encoded
        // at all, or because the encoding was flawed in a way we could
        // not get around.
        $analyzed_word["decoded_word"] = $word;
        continue;
      }

      $analyzed_word["was_encoded"] = TRUE;
      $analyzed_word["decoded_word"] = $decoded_word;
      $analyzed_word["charset"] = $charset;
      $utf8_word = @iconv($charset, "UTF-8", $decoded_word);
      if ($utf8_word === FALSE) {
        // The conversion failed. Maybe it's a workable bug.
        $all_iconv_ok = FALSE;  // We will try to work around this later.
        continue;
      }

      $analyzed_word["encoded_utf8_ok"] = TRUE;
      $analyzed_word["utf8_word"] = $utf8_word;
    }

    return $all_iconv_ok;
  }

  private function build_analyzed_words_2() {
    /*
     * At least one iconv conversion failed. Maybe it's because a multibyte
     * character was incorrectly split in two different encoded-words.
     * Try to concatenate adjacent decoded words with same charset and
     * convert the result at once.
     */
    $this->analyzed_words_2 = $this->analyzed_words;

    for ($idx = 0; $idx < count($this->analyzed_words_2); $idx++) {
      $analyzed_word_2 = &$this->analyzed_words_2[$idx];  // Reference
      if (! $analyzed_word_2["was_encoded"]) {
        // This word is OK.
        continue;
      }
      if ($analyzed_word_2["encoded_utf8_ok"]) {
        // This word is OK.
        continue;
      }
      $decoded_word = $analyzed_word_2["decoded_word"];
      $charset = $analyzed_word_2["charset"];
      $jdx = $idx+1;  // See next word
      while ($jdx < count($this->analyzed_words_2)) {
        $next_word = $this->analyzed_words_2[$jdx];
        if (! $next_word["was_encoded"]) {
          // Too bad, next word was not an encoded-word
          return FALSE;
        }
        if ($next_word["charset"] !== $charset) {
          // Too bad, next encoded-word has not the same charset
          return FALSE;
        }
        // Concatenate both decodings and suppress next word
        $decoded_word .= $next_word["decoded_word"];
        array_splice($this->analyzed_words_2, $jdx, 1);
        $utf8_word = @iconv($charset, "UTF-8", $decoded_word);
        if ($utf8_word !== FALSE) {
          // The conversion succeeded.
          // Update current element and interrupt sub-loop
          $analyzed_word_2["encoded_utf8_ok"] = TRUE;
          $analyzed_word_2["utf8_word"] = $utf8_word;
          break;
        }
      }
      if (! $analyzed_word_2["encoded_utf8_ok"]) {
        // Too bad, we couldn't fix it
        return FALSE;
      }
    }
    // The problem has been fixed
    return TRUE;
  }

  private function finalize($analyzed_words) {
    $result = "";
    $prev_encoded = FALSE;
    foreach ($analyzed_words as $analyzed_word) {
      if (! $analyzed_word["encoded_utf8_ok"]) {
        // Not an valid encoded-word
        if ($result !== "") {
          $result .= " ";
        }
        $result .= $analyzed_word["word"];
        $prev_encoded = FALSE;
      } else {
        // An encoded-word
        if (($result !== "") && (! $prev_encoded)) {
          $result .= " ";
        }
        $result .= $analyzed_word["utf8_word"];
        $prev_encoded = TRUE;
      }
    }
    return $result;
  }
}

?>
