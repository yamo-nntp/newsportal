<?php
/*
 *  NewsPortal: Functions for posting articles to a newsgroup
 *
 *  Copyright (C) 2002-2006 Florian Amrhein
 *  E-Mail: newsportal@florian-amrhein.de
 *  Web: http://florian-amrhein.de
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */



/*
 * generate a message-id for posting.
 * $identity: a string containing informations about the article, to
 *     make a md5-hash out of it.
 *
 * returns: a complete message-id
 */
function generate_msgid($identity) {
  global $msgid_generate,$msgid_fqdn;
  switch($msgid_generate) {
    case "no":
      // no, we don't want to generate a message-id.
      return false;
      break;
    case "md5":
      return '<'.md5($identity).'$1@'.$msgid_fqdn.'>';
      break;
    default:
      return false;
      break;
  }
}

function cancrossport($mygroup,$group){
   global $post_do_not_follow_crosspost,$testgroup;
	if (isset($post_do_not_follow_crosspost) && $post_do_not_follow_crosspost) {
			if (isset($mygroup)){
				$group=$mygroup;
		  		$newsgroups=$group;
			}else{
			  if (strpos($group, ",")>0){
			  	  $groupes=explode(",",$group);
				  for ($i=0; $i<=count($groupes)-1; $i++) {
				  	if(post_possible(trim($groupes[$i])) && testGroup(trim($groupes[$i]))
				  	) {
				  		$group=$groupes[$i];
				  		$newsgroups=$group;
				  		break;
				  	}
			  }
			 }
			}
		} elseif($testgroup) {
	  $newsgroups=testgroups($mygroup);
	} else {
	  $newsgroups=$mygroup;
	}
	return $newsgroups;

}



/*
 * Post an article to a newsgroup
 *
 * $subject: The Subject of the article
 * $from: The authors name and email of the article
 * $newsgroups: The groups to post to
 * $ref: The references of the article
 * $body: The article itself
 */
function message_post($subject,$from,$replyto,$newsgroups,$ref,$body) {
  global $server,$port,$send_poster_host,$organization,$text_error;
  global $file_footer,$www_charset,$spooldir;
  global $msgid_generate,$msgid_fqdn,$secret_header;
  global $npversion,$anonym_mail_newsportal_user,$sethttpuseragent,$myhttpuseragent;
  global $article_textwrap,$debug;
  flush();
  $ns=nntp_open($server,$port);
  /* php8 */
   $vector_ref = isset($ref) && is_array($ref) ? $ref : [];
  if ($ns != false) {
    fputs($ns,"POST\r\n");
    $weg=line_read($ns);
    // No need to encode User-Agent
    // fputs($ns, "User-Agent: NewsPortal/$npversion\n ( https://gitlab.com/yamo-nntp/newsportal/-/tags/ )\r\n");
    fputs($ns, "User-Agent: NewsPortal/$npversion \n ( https://gitlab.com/yamo-nntp/newsportal )\r\n");
    if (trim ($replyto) != ""){
	fputs($ns,'Reply-To: '. $replyto ."\r\n"); 
    }
    fputs($ns,'From: '. $from ."\r\n"); //$from is encoded in post.php
    fputs($ns,'Newsgroups: '.$newsgroups."\r\n");
    fputs($ns,"Mime-Version: 1.0\r\n");
    // this is not flowed
    fputs($ns,"Content-Type: text/plain; charset=".$www_charset."\r\n");
    fputs($ns,"Content-Transfer-Encoding: 8bit\r\n");
    fputs($ns, new MimeEncodeHeader("Subject", $subject) . "\r\n");
    if ($send_poster_host)
      @fputs($ns,'Http-Posting-Host: '.gethostbyaddr(getenv("REMOTE_ADDR"))."\r\n");
    if (count($vector_ref)>0) {
      // strip references
      if(strlen(implode(" ",$vector_ref))>900) {
        $ref_first=array_shift($vector_ref);
        do {
          $vector_ref=array_slice($vector_ref,1);
        } while(strlen(implode(" ",$vector_ref))>800);
        array_unshift($vector_ref,$ref_first);
      }
      $new_ref="";
      $temp_ref="";
      for ($i=0; $i<count($vector_ref); $i++) {
			if (strlen($temp_ref. " ".$vector_ref[$i]) > $article_textwrap){
				$new_ref .= $temp_ref . "\r\n " .$vector_ref[$i] . " ";
				$temp_ref='';
			} else {
				$temp_ref .= $vector_ref[$i] . " " ;
			}
			if ((count($vector_ref) - 1) == $i) {
				$new_ref .= $temp_ref;
			}
      }
      fputs($ns,"References: {$new_ref}\r\n");
    }
    if (isset($secret_header)){
			    fputs($ns,"{$secret_header}\r\n");
    }
    $httpUserAgent =$_SERVER['HTTP_USER_AGENT'];
    if ($httpUserAgent !=''){
    		if(isset($sethttpuseragent) && $sethttpuseragent == true) {
      		fputs($ns, new MimeEncodeHeader($myhttpuseragent, $httpUserAgent ) . "\r\n");
			}
    }
    if ((isset($file_footer)) && ($file_footer!="")) {
      $footerfile=fopen($file_footer,"r");
      $body.="\n".fread($footerfile,filesize($file_footer));
      fclose($footerfile);
    }
    /* php8
    $vector_ref = isset($ref) && is_array($ref) ? $ref : []; */
    if($msgid=generate_msgid($subject. ","
                             . $from . ","
                             . $newsgroups . ","
			     . implode(" ",$vector_ref).","
			     . $body))
      fputs($ns,'Message-ID: '.$msgid."\r\n"); 
    $user_apache= !empty($_SERVER['PHP_AUTH_USER'])
      ? $_SERVER['PHP_AUTH_USER']
      : $_SERVER['REMOTE_USER'];
    if ($user_apache =="")
	$user_apache = "anonymous";
    /*
     * To remove possible mail address in headers for spammers 
     */
    if( (isset($anonym_mail_newsportal_user))  
	 && ($anonym_mail_newsportal_user==true) ) {
    list($mail_from, $domain) = explode('@', $user_apache, 2);
        $user_apache=$mail_from . "@newsportal.invalid";
    }
    fputs($ns,"Newsportal-User: " .trim($user_apache)."\r\n");
    fputs($ns,"Path: newsportal\r\n");
    if( (isset($debug))   && ($debug==true) ) {
        fputs($ns, 'Comments: ' . mb_encode_mimeheader( $subject,mb_internal_encoding(), "Q", " \r\n"). "\r\n");     
	//fputs($ns, 'Original-Subject: '. $subject . "\r\n");
    }
    $body=str_replace("\n.\r","\n..\r",$body);//RFC3977
    $body=str_replace("\r",'',$body);
    $b=explode("\n",$body);
    $body="";
    for ($i=0; $i<count($b); $i++) {
      if ((strpos(substr($b[$i],0,strpos($b[$i]," ")),">") != false) | (strcmp(substr($b[$i],0,1),">") == 0)) {
        $body .= textwrap(stripSlashes($b[$i]),998," \r\n")."\r\n";
      } else {
        $body .= textwrap(stripSlashes($b[$i]),$article_textwrap + 2," \r\n")."\r\n";
      }
    }
    fputs($ns,"\r\n".$body."\r\n.\r\n");
    $message=line_read($ns);
    nntp_close($ns);
  } else {
    $message=$text_error["post_failed"];
  }
  // let thread.php ignore the cache for this group, so this new
  // article will be visible instantly
  $groupsarr=explode(",",$newsgroups);
  foreach($groupsarr as $newsgroup) {
    $cachefile=$spooldir.'/'.$newsgroup.'-cache.txt';
    @unlink($cachefile);
  }
  return $message;
}
?>
