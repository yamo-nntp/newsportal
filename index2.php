<?php
/*
** Use this file if you want to show only 
** Newsgroups active in recent days
**
** rename index.php to list.php  
**
** rename it to index.php
*/
header("Expires: ".gmdate("D, d M Y H:i:s",time()+(3600))." GMT");
include "config.inc.php";
// TODO watch the age of $file_groups
include "auth.inc";
$description=$listactive_description;
include "head.inc"; 
echo '<h1 class="np_index_headline">';
echo htmlspecialchars($title);
echo '</h1>' . $text_index["welcome2"] . $text_index["list"];
include("$file_newsportal");
flush();
$newsgroups=groups_read($server,$port);
echo '<div class="np_index_groups">';
groups_show_red($newsgroups, 3);
echo '</div><div style="clear:both;"> </div>';
include "tail.inc"; 
?>
