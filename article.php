<?php
  // register parameters
  $id=$_REQUEST["id"];
  $group=$_REQUEST["group"];
  include "config.inc.php";
  include "auth.inc";
  include "$file_newsportal";
  $error='';
  $read=true;
  if(trim($id)==''){
  	$error=$text_error["article_not_found"];
   $read=false;
  }
  if(trim($group)==''){
  	$error.=$text_register["no_access_group"];
  	$group=$default_group;//to not be empty
	$backgroup='';
  }else{
    $description=$text_description_article["header"] . $group;
    $backgroup=$text_article["back_to_"] . " ";
    $words=explode('.',$group);
    for($i=0; $i<count($words); $i++) {
	if ($i <count($words) -1){
	        $backgroup = $backgroup .  substr($words[$i], 0, 1);  
	} else{
	        $backgroup = $backgroup . ' ' . $words[$i];  
	}
    }

  }
  $message=false;
  if ($read){
  	header("Expires: ".gmdate("D, d M Y H:i:s",time()+120)." GMT");
  	$message=message_read($id,0,$group);// headerdecode already done!
  }
  if (!$message) {
    header ("HTTP/1.0 404 Not Found");
    $subject=$title;
    $title.=' - ' . $text_error["Title_article_not_found"];
    $description= $text_error["Title_article_not_found"];
    $error=$text_error["article_not_found"];;
    if($ns!=false)
    nntp_close($ns);
  } else {
	$subject=$message->header->subject;// headerdecode is already done!
    	header("Last-Modified: ".date("r", $message->header->date));
    	$title.= ' - '.$subject;
    	//fix the $default_group bug...
    	if (trim($group)==trim($default_group)){
    		if(isset($head->followup)){
    			$lesgroupes = explode(",", $message->header->followup);
    			if(trim($lesgroupes[0]) == "junk" || trim($lesgroupes[0]) == "poster"){
    				$lesgroupes = explode(",", $message->header->newsgroups);
    			}
			}    		
    		else{
    			$lesgroupes = explode(",", $message->header->newsgroups);
    		}
    		$group=trim($lesgroupes[0]);
    	}
  }
  include "head.inc";

// has the user read-rights on this article?
if (!user_can_read($newsgroups)) {
  die("access denied");
}

?>
<h1 id="article_np_article_headline" class="np_article_headline"
><?php echo htmlspecialchars($subject); ?></h1>
<table class="np_buttonbar width_100" id="article"><tr>
<?php
  echo '<td class="np_button"><a class="np_button" href="'.
       $file_index.'">'.$text_thread["button_grouplist"].'</a></td>';
  if($backgroup != ''){
  	echo '<td class="np_button np_button_back_to_group"><a class="np_button" href="'.
       $file_thread.'?group='.urlencode($group).'">' . $backgroup . '</a></td>';
  }
  if ((!$readonly) && ($message) &&
      (!function_exists("npreg_group_has_write_access") ||
             npreg_group_has_write_access($group)))
    echo '<td class="np_button"> <a class="np_button" href="'.
         $file_post.'?type=reply&amp;id='.urlencode($id).
         '&amp;group='.urlencode($group)
          . '">'.$text_article["button_answer"].
         '</a></td>';
   if(function_exists('npreg_user_is_moderator') && npreg_user_is_moderator($group)) {
     echo '<td class="np_button"><a class="np_button" href="'.$file_cancel.'?type=reply&amp;id='.urlencode($id).
          '&amp;group='.urlencode($group).'">'.$text_article["button_cancel"].'</a></td>';
    }
 echo '<td class="width_100">&nbsp;</td></tr></table>';
  if (!$message){
    // article not found
    if(trim($error)==''){
    	echo $text_error["article_not_found"];
    } else{
    		echo $error;
    	}
    } else {
    if($article_showthread)
      $thread=thread_cache_load($group);
    //echo "<br>";
    message_show($group,$id,0,$message); // headers readable headerdecode already done!
    if($article_showthread)
      message_thread($message->header->id,$group,$thread); 
  }
  include "tail.inc";
?>
