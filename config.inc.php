<?php
/*
 * Works on PHP 7.4 (debian bullseye) and with bugs on the old PHP5.5
 * works also on PHP 8.1 (ubuntu 22.04) and PHP 7.3 (debian buster)
 * should work on 5.6, 7.2, 8.0 and 8.2
 *
 */
$npversion='0.52.a9';
$debug=false;
/*
 * directories and files
 */
// $spooldir="spool"; // could be outside the DocumentRoot
// by example $spooldir="../spool"  $spooldir="/path/to/spool"
$imgdir="img";
// newsportal.php on 0.51 moved on lib directory!
$file_newsportal="lib/newsportal.php";
$file_index="index.php";
$file_thread="thread.php";
$file_article="article.php";
// $file_article_full="article.php";
$file_attachment="attachment.php";
$file_post="post.php";
$file_cancel="cancel.php";
//$file_language="lang/english.lang";
$file_language="lang/francais.lang";
// $file_footer="footer.txt"; //inutile
$file_footer="";
/*
 * $file_groups
 * like spool could be outside the DocumentRoot 
 * by example $file_groups="../groups.txt"
 * You can find some examples in extras/groups directory
*/
$file_groups="groups.txt";

/* 
 * newsserver setup
 */
// $server="news.florian-amrhein.de";
// $server="creak.um.maine.edu";
//$server="localhost";
$server="127.0.0.1";
$port=119;
// $post_server="";
// $post_port=119;
$maxfetch=0; // depricated
$initialfetch=0;  // depricated
$server_auth_http=true;  // You may uncomment it if you set a basic apache authenfication (NOT TESTED)

/*
 * Grouplist Layout
 */
$gl_age=true;
$default_group='local.test'; // if $group is empty show this group.

/*
 * Thread layout
 */
$thread_treestyle=7;
$fmt_date_short="j M";
$fmt_date="j M y";
$fmt_time="H:i";
$thread_show["date"]=true;
$thread_show["subject"]=true;
$thread_show["author"]=true;
$thread_show["authorlink"]=false;
$thread_show["replies"]=true;
$thread_show["lastdate"]=false; // makes only sense with $thread_show["replies"]=false
$thread_show["threadsize"]=true; // not visible on smartphones.
//$thread_maxSubject=64; // Not very usefull with actual CSS
$thread_maxSubject=0;
$maxarticles=8000;// warning : 0 may be too slow and 8000 a little slow
//$maxarticles=1000;
$maxarticles_extra=600;
$age_count=3;
$age_time[1]=86400; //24 hours
$age_color[1]="red";
$age_time[2]=259200; //3 days
// Modif suggérée par Julien Élie
$age_color[2]="fuchsia";
// Modif suggérée par Julien Élie
$age_time[3]=604800; //7 days
// Modif suggérée par Julien Élie
$age_color[3]="teal";
$thread_sort_order=-1;
$thread_sort_type="thread";
$articles_per_page=250;
$startpage="first";

/* 
 * Frames 
 */
// for frames-support: read README in the frames-directory
//$frame_article="article";
//$frame_thread="thread";
//$frame_groups="_top";
//$frame_post="_top";
//$frame_threadframeset="_top";
$frame_externallink="_blank";

/* 
 * article layout 
 */
$article_show["Subject"]=true;
$article_show["From"]=true;
$article_show["Newsgroups"]=true;
$article_show["Followup"]=true;
$article_show["Organization"]=true;
$article_show["Date"]=true;
$article_show["Message-ID"]=true;// link to http://al.howardknight.net/
$article_show["User-Agent"]=true;
$article_show["References"]=true;
$article_show["From_link"]=false;
$article_show["From_rewrite"]=array('@',' (at) *nospam* ');
$article_showthread=true;
$article_graphicquotes=true;
$article_textwrap=74; // if $article_wordwrap =true
$article_wordwrap=false;

/*
 * settings for the article flat view, if used
 */
$articleflat_articles_per_page=10;
$articleflat_chars_per_articles=500; //not used?

/*
 * Message posting
 */
$send_poster_host=true;
/*
 * SECURITY
 */
$readonly=true; // If false post is possible....
$testgroup=true; // don't disable unless you really know what you are doing!
// $testgroup=false;  //all newsgroups can be viewed
// $testgroup=true; // default setting : only listed newsgroups can be viewed
$validate_email=1;
//$organization= 'http://" .$_SERVER["HTTP_HOST"];
//$organization= $_SERVER["HTTP_HOST"];
$sethttpuseragent=true;
$myhttpuseragent="Http-User-Agent";
$setcookies=true;
//$anonym_address="user@tld.invalid";
// $secret_header="myheader_secret: secret";// for NNRPD filter Edit it if it is useful for you.
// $set_replyto=false // do not use the mail field for reply-to
$set_replyto=true; // makes only sense if $anonym_address isset !
$anonym_mail_newsportal_user=false; // Newsportal-User header
//$msgid_generate="md5";
$msgid_generate="no"; // not tested in this version
// Modif suggérée par Julien Élie
if (array_key_exists('HTTP_HOST', $_SERVER)) {
     $msgid_fqdn=$_SERVER['HTTP_HOST'];
} else {
     $msgid_fqdn='localhost.local';
}

$post_autoquote=true;
$post_captcha=false;
$post_do_not_follow_crosspost=true; //Reply only to the group where you are!
// bug in this version : $post_do_not_follow_crosspost=false DO NOT WORK!
/* 
 * Attachments
 */
$attachment_show=true;
$attachment_delete_alternative=true; // delete non-text mutipart/alternative
$attachment_uudecode=false;  // experimental!

/*
 * Security settings
 */
$block_xnoarchive=false;

/*
 * User registration and database
 */
// $npreg_lib="lib/npreg.inc.php";

/*
 * Cache
 */
$cache_articles=false;  // article cache, experimental in v0.38 and may be not useful on a modern Apache
$cache_index=3600; // cache the group index for one hour before reloading from NNTP server
$cache_thread=60; // cache the thread for one minute reloading from NNTP server

/*
 * Misc 
 */
// $title="Newsportal $npversion USENET";// verbose title
$title="Newsportal USENET";
$cutsignature=true;
$compress_spoolfiles=false; // true may cause bugs...

/*
 * ENCODING, LANG AND TIMEZONE SETTINGS
 */
mb_internal_encoding('UTF-8');
$www_charset = mb_internal_encoding(); // should be UTF-8. You can edit it if you have a good reason.
// Use the iconv extension for improved charset conversions
$iconv_enable=true;
// timezone relative to GMT, +1 for CET
//$timezone=-5;
$myLC_TIME="fr_FR"; // To be modified or moved in /home/newsportal/local.inc.php
$myHTML_lang="fr"; // To be modified
$timezone=+2; // To be modified
$minzone=0; // To be modified

/*
 * Group specific config 
 */
//$group_config=array(
//  '^de\.alt\.fan\.aldi$' => "aldi.inc",
//  '^de\.' => "german.inc"
//);

/*
 * Local configuration
 * to keep important things when updating newsportal
 */
//include "local.inc.php";
//include "/somewhere/local.inc.php";
// Very local for an instance with you have multiple VirtualHosts
//include "/somewhere/local-test.inc.php";
// *******

/*
 * Do not edit anything below this line
 */
// Load group specifig config files
if((isset($group)) && (isset($group_config))) {
  foreach ($group_config as $key => $value) {
    if (preg_match($key,$group)) {
      include $value;
      break;
    }
  }
}

// check the settings
include "lib/check.php";

// load the english language definitions first because some of the other
// definitions are incomplete
include("lang/english.lang"); 
include($file_language);
?>
