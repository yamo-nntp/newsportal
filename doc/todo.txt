The following features may be available in NewsPortal at some point :

- Possibility for the user, the thread view and the one used Change language.
- configuration script
- Setting options, when and how often a connection is made to the news server
   to see for new articles etc. At the moment we are constantly reconnecting,
   which is not exactly beneficial for the speed, and the
   Newsserver loaded unnecessarily.
- Query Reply-To and display accordingly
- enable and control cross-posting?
- You should be able to set a "Followup-To: poster" header
- Posting of articles that must only contain quoted lines
   be prevented. (This can be done on the news server)
- Keep the load of the news server lower (e.g. by caching)
